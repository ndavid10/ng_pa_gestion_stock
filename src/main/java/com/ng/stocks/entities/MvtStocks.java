package com.ng.stocks.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MvtStocks implements Serializable{
	private static final int ENTREE=1;
	private static final int SORTIE=2;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idMvtStock;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateDuMvt;
	private BigDecimal quantite;
	private int typeMvt;
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	private String codeUtilisateur;

	public MvtStocks() {
		super();
	}
	
	
	
	public String getCodeUtilisateur() {
		return codeUtilisateur;
	}

	public void setCodeUtilisateur(String codeUtilisateur) {
		this.codeUtilisateur = codeUtilisateur;
	}



	public Date getDateDuMvt() {
		return dateDuMvt;
	}



	public void setDateDuMvt(Date dateDuMvt) {
		this.dateDuMvt = dateDuMvt;
	}



	public BigDecimal getQuantite() {
		return quantite;
	}



	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}



	public int getTypeMvt() {
		return typeMvt;
	}



	public void setTypeMvt(int typeMvt) {
		this.typeMvt = typeMvt;
	}



	public Article getArticle() {
		return article;
	}



	public void setArticle(Article article) {
		this.article = article;
	}



	public Long getIdMvtStock() {
		return idMvtStock;
	}

	public void setIdMvtStock(Long idMvtStock) {
		this.idMvtStock = idMvtStock;
	}
	
	

}
