package com.ng.stocks.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCdesClients implements Serializable{
	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	private Long idLigneCdesClients;
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name = "idCdeClient")
	private CommandesClients commandesClients;
	

	public LigneCdesClients() {
		super();
	}
	
	

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandesClients getCommandesClients() {
		return commandesClients;
	}

	public void setCommandesClients(CommandesClients commandesClients) {
		this.commandesClients = commandesClients;
	}

	public Long getIdLigneCdesClients() {
		return idLigneCdesClients;
	}

	public void setIdLigneCdesClients(Long idLigneCdesClients) {
		this.idLigneCdesClients = idLigneCdesClients;
	}
	
		
}
