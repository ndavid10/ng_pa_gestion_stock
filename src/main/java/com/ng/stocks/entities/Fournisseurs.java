package com.ng.stocks.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Fournisseurs implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idFournisseurs;
	private String nom;
	private String prenom;
	private String adresse;
	private String photo;
	private String email;
	private String telephone;
	private String codeFournisseur;
	@OneToMany(mappedBy = "fournisseurs",fetch = FetchType.LAZY)
	private List<CommandesFournisseurs> commandesFournisseurs;

	public Fournisseurs() {
		super();
	}
	
	

	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getPrenom() {
		return prenom;
	}



	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	public String getAdresse() {
		return adresse;
	}



	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}



	public String getPhoto() {
		return photo;
	}



	public void setPhoto(String photo) {
		this.photo = photo;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getTelephone() {
		return telephone;
	}



	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}



	public String getCodeFournisseur() {
		return codeFournisseur;
	}



	public void setCodeFournisseur(String codeFournisseur) {
		this.codeFournisseur = codeFournisseur;
	}



	public List<CommandesFournisseurs> getCommandesFournisseurs() {
		return commandesFournisseurs;
	}



	public void setCommandesFournisseurs(List<CommandesFournisseurs> commandesFournisseurs) {
		this.commandesFournisseurs = commandesFournisseurs;
	}



	public Long getIdFournisseurs() {
		return idFournisseurs;
	}

	public void setIdFournisseurs(Long idFournisseurs) {
		this.idFournisseurs = idFournisseurs;
	}
	
	
	
}
