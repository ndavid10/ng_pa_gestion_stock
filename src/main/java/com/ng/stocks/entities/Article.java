package com.ng.stocks.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Article implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idArticle;
	private String designation;
	private String codeAriticle;
	private BigDecimal prixUnitaire;
	private BigDecimal tauxTva;
	private BigDecimal prixUnitaireHT;
	private BigDecimal prixUnitaireTTC;
	@OneToMany(mappedBy = "article",fetch = FetchType.LAZY)
	private List<MvtStocks> mvtStocks;
	@OneToMany(mappedBy = "article",fetch = FetchType.LAZY)
	private List<LigneCdesClients> ligneCdesClients;
	@OneToMany(mappedBy = "article",fetch = FetchType.LAZY)
	private List<LigneCdesFournisseurs> ligneCdesFournisseurs;
	@OneToMany(mappedBy = "article",fetch = FetchType.LAZY)
	private List<LingesVentes> lingesVentes;
	private String photo;
	@ManyToOne
	@JoinColumn(name = "idCategorie")
	private Categories categories;
	
	public Article() {
	}

	
	public List<MvtStocks> getMvtStocks() {
		return mvtStocks;
	}


	public void setMvtStocks(List<MvtStocks> mvtStocks) {
		this.mvtStocks = mvtStocks;
	}


	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getCodeAriticle() {
		return codeAriticle;
	}

	public void setCodeAriticle(String codeAriticle) {
		this.codeAriticle = codeAriticle;
	}

	public BigDecimal getPrixUnitaire() {
		return prixUnitaire;
	}

	public void setPrixUnitaire(BigDecimal prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}

	public BigDecimal getTauxTva() {
		return tauxTva;
	}

	public void setTauxTva(BigDecimal tauxTva) {
		this.tauxTva = tauxTva;
	}

	public BigDecimal getPrixUnitaireHT() {
		return prixUnitaireHT;
	}

	public void setPrixUnitaireHT(BigDecimal prixUnitaireHT) {
		this.prixUnitaireHT = prixUnitaireHT;
	}

	public BigDecimal getPrixUnitaireTTC() {
		return prixUnitaireTTC;
	}

	public void setPrixUnitaireTTC(BigDecimal prixUnitaireTTC) {
		this.prixUnitaireTTC = prixUnitaireTTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public Long getIdArticle() {
		return idArticle;
	}

	public void setIdArticle(Long idArticle) {
		this.idArticle = idArticle;
	}
	
	

}
