package com.ng.stocks.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandesClients implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCdeClient;
	private String codeCommande;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommande;
	@ManyToOne
	@JoinColumn(name = "idClient")
	private Clients clients;
	@OneToMany(mappedBy = "commandesClients",fetch = FetchType.LAZY)
	private List<LigneCdesClients> ligneCdesClients;
	private String codeUtilisateur;
	
	public CommandesClients() {
		super();
	}

	
	public String getCodeUtilisateur() {
		return codeUtilisateur;
	}


	public void setCodeUtilisateur(String codeUtilisateur) {
		this.codeUtilisateur = codeUtilisateur;
	}


	public Long getIdCde() {
		return idCdeClient;
	}

	public void setIdCde(Long idCde) {
		this.idCdeClient = idCde;
	}

	public Long getIdCdeClient() {
		return idCdeClient;
	}

	public void setIdCdeClient(Long idCdeClient) {
		this.idCdeClient = idCdeClient;
	}

	public String getCodeCommande() {
		return codeCommande;
	}

	public void setCodeCommande(String codeCommande) {
		this.codeCommande = codeCommande;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public Clients getClients() {
		return clients;
	}

	public void setClients(Clients clients) {
		this.clients = clients;
	}

	public List<LigneCdesClients> getLigneCdesClients() {
		return ligneCdesClients;
	}

	public void setLigneCdesClients(List<LigneCdesClients> ligneCdesClients) {
		this.ligneCdesClients = ligneCdesClients;
	}
	
	

}
