package com.ng.stocks.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Ventes implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idVentes;
	private String codeVente;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateVente;
	@OneToMany(mappedBy = "ventes",fetch = FetchType.LAZY)
	private List<LingesVentes> lingesVentes;
	private String codeUtilisateur;

	public Ventes() {
		super();
	}


	public String getCodeVente() {
		return codeVente;
	}


	public void setCodeVente(String codeVente) {
		this.codeVente = codeVente;
	}


	public Date getDateVente() {
		return dateVente;
	}


	public void setDateVente(Date dateVente) {
		this.dateVente = dateVente;
	}


	public List<LingesVentes> getLingesVentes() {
		return lingesVentes;
	}


	public void setLingesVentes(List<LingesVentes> lingesVentes) {
		this.lingesVentes = lingesVentes;
	}


	public String getCodeUtilisateur() {
		return codeUtilisateur;
	}


	public void setCodeUtilisateur(String codeUtilisateur) {
		this.codeUtilisateur = codeUtilisateur;
	}


	public Long getIdVentes() {
		return idVentes;
	}

	public void setIdVentes(Long idVentes) {
		this.idVentes = idVentes;
	}
	


}
