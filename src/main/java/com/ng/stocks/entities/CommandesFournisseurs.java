package com.ng.stocks.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CommandesFournisseurs implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCdesFournisseurs;
	@ManyToOne
	@JoinColumn(name = "idFournisseurs")
	private Fournisseurs fournisseurs;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCommande;
	@OneToMany(mappedBy = "commandesFournisseurs",fetch = FetchType.LAZY)
	private List<LigneCdesFournisseurs> ligneCdesFournisseurs;
	private String codeUtilisateur;
	

	public CommandesFournisseurs() {
		super();
			}
	
	public String getCodeUtilisateur() {
		return codeUtilisateur;
	}

	public void setCodeUtilisateur(String codeUtilisateur) {
		this.codeUtilisateur = codeUtilisateur;
	}

	public Fournisseurs getFournisseurs() {
		return fournisseurs;
	}

	public void setFournisseurs(Fournisseurs fournisseurs) {
		this.fournisseurs = fournisseurs;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public List<LigneCdesFournisseurs> getLigneCdesFournisseurs() {
		return ligneCdesFournisseurs;
	}

	public void setLigneCdesFournisseurs(List<LigneCdesFournisseurs> ligneCdesFournisseurs) {
		this.ligneCdesFournisseurs = ligneCdesFournisseurs;
	}

	public Long getIdCdesFournisseurs() {
		return idCdesFournisseurs;
	}

	public void setIdCdesFournisseurs(Long idCdesFournisseurs) {
		this.idCdesFournisseurs = idCdesFournisseurs;
	}
}
