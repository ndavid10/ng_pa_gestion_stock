package com.ng.stocks.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LigneCdesFournisseurs implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idLigneCdesFournisseurs;
	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	@ManyToOne
	@JoinColumn(name = "idCdesFournisseurs")
	private CommandesFournisseurs commandesFournisseurs;

	public LigneCdesFournisseurs() {
		super();
	}
	
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}


	public CommandesFournisseurs getCommandesFournisseurs() {
		return commandesFournisseurs;
	}

	public void setCommandesFournisseurs(CommandesFournisseurs commandesFournisseurs) {
		this.commandesFournisseurs = commandesFournisseurs;
	}

	public Long getIdLigneCdesFournisseurs() {
		return idLigneCdesFournisseurs;
	}

	public void setIdLigneCdesFournisseurs(Long idLigneCdesFournisseurs) {
		this.idLigneCdesFournisseurs = idLigneCdesFournisseurs;
	}
	
	

}
